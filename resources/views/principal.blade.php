<!DOCTYPE html>
<html lang="es">
@include('layouts.header')
<body>
<div id="app">
    @include('layouts.menu')
    <div class="container-fluid mb-5">
        @include('layouts.spinner')
        @yield('contenido')
        @yield('componentes')
    </div>
</div>
@include('layouts.footer')
@include('layouts.scripts')
</body>
</html>
