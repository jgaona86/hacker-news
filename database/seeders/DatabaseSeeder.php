<?php

namespace Database\Seeders;

use App\Models\Noticia;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        DB::table('noticias')->truncate();
        DB::table('noticias')->insert([
            ['id' => 1, 'nombre' => 'Andrea Valdiri confesó', 'descripcion' => 'La bailarina barranquillera estuvo conversando en un Instagram Live con ‘Epa Colombia’ que ha dado mucho de qué hablar.',  'url' => 'https://www.infobae.com/america/colombia/2021/05/27/andrea-valdiri-confeso-que-ha-sido-lo-mas-ridiculo-que-ha-hecho-por-amor/'],
            ['id' => 2, 'nombre' => 'En los próximos días se inicia la producción local de la Sputnik V', 'descripcion' => 'El laboratorio indicó que "en los próximos días, cumplimentado los requerimientos regulatorios, se podrá iniciar la producción para poner a disposición de los argentinos un número importante de vacunas".',  'url' => 'https://www.telam.com.ar/notas/202106/556399-argentina-laboratorio-richmond-vacuna-sputnik-v-coronavirus.html'],
            ['id' => 3, 'nombre' => 'Argentina está entre los países de la región que recibirán 6 millones de dosis de EEUU', 'descripcion' => 'La Casa Blanca anunció cómo Estados Unidos distribuirá las vacunas que no utiliza para su campaña de inmunización y destacó que entregará seis millones de dosis para el mecanismo Covax para distribuir a al menos 12 países de América Latina.',  'url' => 'https://www.telam.com.ar/notas/202106/556466-argentina-vacunas-entrega-estados-unidos.html'],
            ['id' => 4, 'nombre' => 'Con Messi desde el comienzo', 'descripcion' => 'El equipo "albiceleste" y los trasandinos disputarán, desde las 21, uno de los cuatro duelos por la jornada de clasificatorias para la Copa del Mundo de Qatar 2022, tras casi siete meses de inactividad a causa de la pandemia.',  'url' => 'https://www.telam.com.ar/notas/202106/556348-con-messi-en-el-seleccionado-argentina-recibe-a-chile-en-santiago-del-estero.html'],
            ['id' => 5, 'nombre' => 'África no está preparada para una tercera ola de coronavirus', 'descripcion' => 'Muchos hospitales y clínicas están lejos de estar preparadas para encarar un aumento drástico del número de pacientes gravemente enfermos", indicó Matshidiso Moeti, directora regional del organismo.',  'url' => 'https://www.telam.com.ar/notas/202106/556469-africa-nuevos-casos-coronavirus.html'],

            ['id' => 6, 'nombre' => 'Detuvieron a un hombre que confesó haber asesinado a su pareja en Merlo', 'descripcion' => 'Un hombre fue apresado tras reconocer ante la Policía haber asesinado a su mujer, cuyo cadáver fue encontrado en un descampado con golpes en la cabeza y cortes en distintas partes del cuerpo.',  'url' => 'https://www.telam.com.ar/notas/202106/556422-femicidio-merlo-cadaver-descampado.html'],
            ['id' => 7, 'nombre' => 'Denuncian que Larreta excluyó de la vacunación a docentes y no docentes de universidades', 'descripcion' => 'Las autoridades de la Universidad de Buenos Aires, la Universidad Nacional de las Artes y la Universidad Tecnológica Nacional plantearon al ministro Nicolás Trotta que el Gobierno porteño no cumple lo acordado en el Consejo Federal. ',  'url' => 'https://www.telam.com.ar/notas/202106/556468-vacunacion-docentes-universitarios.html'],
            ['id' => 8, 'nombre' => 'Es Ley la reforma de Ganancias para Empresas', 'descripcion' => 'Por 36 votos a favor y 26 en contra, se aprobó en el Senado el proyecto que establece una nueva estructura de alícuotas escalonadas con tres segmentos en relación al nivel de ganancia neta imponible acumulada.',  'url' => 'https://www.telam.com.ar/notas/202106/556387-ley-reforma-ganancias-empresas.html'],
            ['id' => 9, 'nombre' => 'Perotti dijo que Santa Fe quiere comprar vacunas pero no hay disponibles', 'descripcion' => 'El gobernador aseguró que las gestiones se iniciaron hace más de dos meses y advirtió como una de las dificultades que se presentan que "hay muchos intermediarios que ofrecen lo que no tienen".',  'url' => 'https://www.telam.com.ar/notas/202106/556184-compra-vacunas-coronavirus-santa-fe-omar-perotti.html'],
            ['id' => 10,'nombre' => 'De vuelta tras la pandemia, el Festival de Cannes anunció su Selección Oficial', 'descripcion' => 'La edición 74ta. de la clásica muestra francesa de cine volverá al ruedo entre el 6 y el 17 de julio. El jurado estará presidido por Spike Lee.',  'url' => 'https://www.telam.com.ar/notas/202106/556463-cannes-festival-cine-premio.html'],


        ]);
    }
}
