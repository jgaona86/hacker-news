<p align="center"><a href="#" ><img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQFYlSA85RyAQFvVZoySujrhNCJ_E1VGTajs186-t9Crshyvq3-jvHUv7YpqB6k_HYoriU&usqp=CAU" width="300"></a></p>

# HACKERS-NEWS
El presente software es una muestra técnica de un sistema con autenticación por medio de login y un ABM Noticias.
muestra las últimas 10 noticias publicadas en HackerNews
## Requerimientos
Se detallan los requerimientos de las principales tecnologias utilizadas, las demos requerimientos se detallan 
en el package.json y composer.json, para las demas dependencias.

- Apache            versión: 2.4.46
- Php               versión: 7.3.21
- Mysql             versión: 8.0.21
- Laravel Framework versión: 8.44.0
- npm               versión: 6.14.6
- Vue Js            versión: 2.5.17

## Desarrollador

- Nombre: **Jaime Gaona**
- Email:  **jagquiroga@gmail.com**

## Guía de instalación

1. clona este repositorio en la carpeta destino donde tu servidor pueda leer los archivos.

```
git clone https://jgaona86@bitbucket.org/jgaona86/hacker-news.git
```
2. Configura las variables de conexión a la base de datos en .env - en el .env.example encontra una copia de la
correcta configuración

```  
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=hacker_news
DB_USERNAME=admin
DB_PASSWORD=admin123  
```  

3. Compruebe que todos sus servicios se encuentren corriendo (Servidor de base de datos, Apache, etc.)

4. instalar la dependencias de php

```
composer install
```

5. Instalar las dependencias de npm

```
npm install
```

6. levantar la base de datos con el seeder de noticias

```
php artisan migration:fresh --seed
```
7. Recuerde que para acceder al proyecto si realiza la configuracion de Vhost directamente podra ver corriendo
el proyecto a la url que asigno si no debera correr para un entorno local

Compila el FE con la posibilidad de estar observado cada cambio realizado
```
npm run watch
```

levanta el servidor en el puerto 8000 por defaul
```
php artisan run serve
```

Para acceder estando en un ambiente local con los comandos anteriores: http://localhost:8000/.

### Por default se han registrado 10 Noticias:

- **Noticia:** Andrea Valdiri confesó
- **Noticia:** En los próximos días se inicia la producción local de la Sputnik V
- **Noticia:** Argentina está entre los países de la región que recibirán 6 millones de dosis de EEUU
- **Noticia:** Con Messi desde el comienzo
- **Noticia:** África no está preparada para una tercera ola de coronavirus
- **Noticia:** Detuvieron a un hombre que confesó haber asesinado a su pareja en Merlo
- **Noticia:** Denuncian que Larreta excluyó de la vacunación a docentes y no docentes de universidades
- **Noticia:** Es Ley la reforma de Ganancias para Empresas
- **Noticia:** Perotti dijo que Santa Fe quiere comprar vacunas pero no hay disponibles
- **Noticia:** De vuelta tras la pandemia, el Festival de Cannes anunció su Selección Oficial
