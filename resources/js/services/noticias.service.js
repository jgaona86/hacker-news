import axios from 'axios'

class NoticiasService {
    guardarNoticia(data) {
        let url = '/noticias'
        return axios.post(url, {
            nombre: data.nombre,
            descripcion: data.descripcion,
            url: data.url,
        })
    }
    updateNoticia(objeto, id) {
        return axios.put('/noticias/'+id, {objeto})
    }
}

export default new NoticiasService()
