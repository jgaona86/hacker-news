import Vue from 'vue'
import Vuex from 'vuex'
import services from '../services'
Vue.use(Vuex)
export default new Vuex.Store({
    state: {
        loading: false,
        services,
        axiosCacheable: false,
        parametros: [],
    },
    mutations: {
        loading (state, loading) {
            state.loading = loading
        },
        axiosCacheable (state, cacheable) {
            state.axiosCacheable = cacheable
        },
    },
    actions: {
    },
    modules: {
    },
})
