<?php

namespace App\Http\Controllers;

use App\Models\Noticia;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class NoticiaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function index()
    {
        try {

            $rs = DB::table('noticias')
                ->select(
                    'noticias.id as id',
                    'noticias.nombre as nombre',
                    'noticias.descripcion as descripcion',
                    'noticias.url as url',
                    'noticias.favorita as favorita',
                    'noticias.created_at as created_at',
                    'noticias.updated_at as updated_at',
                )
                ->limit(10)
                ->orderBy('created_at', 'desc')
                ->get();

            return $rs->toJson();

        } catch (\Throwable $th) {
            return response()->json([
                'code' => 500,
                'message' => 'Se produjo un error.',
                'data' => ['detail' => $th->getMessage()]],
                500);
        }
    }

    /**
     * Obtener las ultimas 10 noticias
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\JsonResponse
     */
    public function obtenerFavoritas()
    {

        try {
            $rs = DB::table('noticias')
                ->select(
                    'noticias.id as id',
                    'noticias.nombre as nombre',
                    'noticias.descripcion as descripcion',
                    'noticias.url as url',
                    'noticias.favorita as favorita',
                    'noticias.created_at as created_at',
                    'noticias.updated_at as updated_at',
                )
                ->limit(10)
                ->orderBy('created_at', 'desc')
                ->where('favorita', '=', 1)
                ->get();

            return $rs->toJson();

        } catch (\Throwable $th) {
            return response()->json([
                'code' => 500,
                'message' => 'Se produjo un error.',
                'data' => ['detail' => $th->getMessage()]],
                500);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('noticias.nuevo');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $val = Validator::make($request->All(), [
                "nombre" => 'present|max:100',
                "descripcion" => 'present|max:255',
                "url" => 'present|max:255',
            ], [], [
                "nombre" => "Nombre",
                "descripcion" => "Descripcion",
                "url" => "Url",
            ]);

            if ($val->fails()) {
                return response()->json([
                    'code' => 422,
                    'message' => 'Se produjo un error de validación.',
                    'data' => $val->errors()],
                    422);
            }

            $noticia = $request->All();
            $objNoticia = Noticia::create($noticia);

            return response()->json([
                'code' => 200,
                'message' => 'La operación ha finalizado exitosamente.'], 200);

        } catch (\Throwable $th) {
            //DB::unprepared("UNLOCK TABLES");
            //DB::rollBack();
            return response()->json([
                'code' => 500,
                'message' => 'Se produjo un error.',
                'data' => ['detail' => $th->getMessage()]], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $noticia = Noticia::findOrFail($id);

        return view('noticias.editar', compact('noticia'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();

        $nombre = $data['objeto']['nombre'];
        $descripcion = $data['objeto']['descripcion'];
        $url = $data['objeto']['url'];
        $favorita = $data['objeto']['favorita'];

        $obj = Noticia::findOrFail($id);

        $obj->nombre = $obj->nombre != $nombre ? $nombre : $obj->nombre;
        $obj->descripcion = $obj->descripcion != $descripcion ? $descripcion : $obj->descripcion;
        $obj->url = $obj->url != $url ? $url : $obj->url;
        $obj->favorita = $obj->favorita != $favorita ? $favorita : $obj->favorita;

        $obj->update();
        return response()->json(['code' => 200, 'message' => 'La operación ha finalizado exitosamente.'], 200);
    }

    public function updateFavorita(Request $request, $id){

        $data = $request->all();
        $favorita = $data['objeto']['favorita'];

        $obj = Noticia::findOrFail($id);
        $obj->favorita = $obj->favorita != $favorita ? $favorita : $obj->favorita;
        $obj->update();

        return response()->json(['code' => 200, 'message' => 'La operación ha finalizado exitosamente.'], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
