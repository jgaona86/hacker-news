/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

Vue.component('example-component', require('./components/ExampleComponent.vue').default);

//Noticias
Vue.component('form-noticias-component', require('./components/noticias/FormComponent').default);
Vue.component('noticias-public-component', require('./components/noticias/NoticiasPublicComponent').default);
Vue.component('noticias-listado-component', require('./components/noticias/ListadoComponent').default);
Vue.component('favoritas-public-component', require('./components/noticias/FavoritasPublicComponent').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import Vuelidate from 'vuelidate'
import store from './store/index.js'

Vue.use(BootstrapVue)
Vue.use(IconsPlugin)
Vue.use(Vuelidate)
window.onload = function () {
    sessionStorage.clear()
}

const app = new Vue({
    el: '#app',
    store
});
