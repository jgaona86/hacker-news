<?php

use App\Http\Controllers\NoticiaController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/home/favoritas', [App\Http\Controllers\HomeController::class, 'favoritas'])->name('favoritas');
Route::get('/noticias/favoritas', [NoticiaController::class, 'obtenerFavoritas']);

//noticias
Route::resource('/noticias', NoticiaController::class)->middleware('auth');

Route::put('/noticias/favoritas/{id}', [NoticiaController::class, 'updateFavorita']);
Route::get('/noticias', [NoticiaController::class, 'index']);




