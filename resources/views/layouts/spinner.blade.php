{{-- Spinner --}}
<div class="modal" style="display: none;" id="spinner_principal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="spinner-border text-primary" style="margin-left:auto; margin-right: auto">
        </div>
    </div>
</div>
{{-- Bootstrap-Vue Spinner --}}
<b-overlay :show="$store.state.loading" no-wrap variant="dark" height="50px" opacity="0.5" spinner-variant="primary" fixed z-index="9999"></b-overlay>

