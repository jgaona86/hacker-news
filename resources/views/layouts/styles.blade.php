<link href="{{ asset('css/app.css') }}" rel="stylesheet">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
<style>
    html {
        position: relative;
        min-height: 100%;
    }
    body {
        margin-bottom: 60px;
    }
    .app-footer {
        position: absolute;
        bottom: 0;
        width: 100%;
        min-height: 3em;
        line-height: 1em;
        background-color: #007bff !important;
        padding-left: 0.5em;
        padding: 0.5em;
    }
</style>
